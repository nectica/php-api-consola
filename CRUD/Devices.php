<?php 
include '../models/DeviceModel.php';
include '../core/Response.php';
$devices = new DeviceModel();
$status = 'success';
$now = date("Y-m-d H:i:s");
$responseExample = [
    ["id" => 0 , 'id_status' => 0, "nombre_operador" => 'Jose hernandez', 'id_objetivo' => 0 , "nombre_objetivo" => 'objetivo ronda', 'tipo_evento' => "evento ronda","status" => 'danger', 'fecha' => $now ],
    ["id" => 1 , 'id_status' => 1, "nombre_operador" => 'lautaro fernandez', 'id_objetivo' => 1 , "nombre_objetivo" => 'objetivo alive Man', 'tipo_evento' => "evento alive Man","status" => 'danger', 'fecha' => $now],
    ["id" => 2 , 'id_status' => 2, "nombre_operador" => 'eduardo rodriguez', 'id_objetivo' => 2 , "nombre_objetivo" => 'objetivo todo ok esta', 'tipo_evento' => "evento todo ok esta","status" => 'danger', 'fecha' => $now],
    ["id" => 3 , 'id_status' => 3, "nombre_operador" => 'benjamin rauseo', 'id_objetivo' => 3 , "nombre_objetivo" => 'objetivo novedades', 'tipo_evento' => "evento novedades","status" => 'danger', 'fecha' => $now],
    ["id" => 4 , 'id_status' => 4, "nombre_operador" => 'emilio avila', 'id_objetivo' => 4 , "nombre_objetivo" => 'objetivo alerta', 'tipo_evento' => "evento alerta","status" => 'danger', 'fecha' => $now],
    ["id" => 5 , 'id_status' => 5, "nombre_operador" => 'laureano marquez', 'id_objetivo' => 5 , "nombre_objetivo" => 'objetivo no completo', 'tipo_evento' => "evento no completo","status" => 'danger', 'fecha' => $now ],
];
$aux = [];
$data = [];
$id = 0 ;
if(isset($_GET["id"])  ){
    $id = $_GET["id"];   
}
$numberRecords = random_int(1, 100);
for ($i=0; $i < $numberRecords; $i++) { 
    $randomIndex =  random_int(0, (count($responseExample ) - 1)  );
    $responseExample[$randomIndex]["id"] = $id + $i + 1 ;
    array_unshift($aux, $responseExample[$randomIndex]);
}
$data = $aux;
$response = compact('status', 'data') ;
Response::json($response);
