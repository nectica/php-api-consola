<?php

include '../models/EventosModel.php';
include '../core/Response.php';
$eventos = new EventosModel();
$status = "success";
$now = date("Y-m-d H:i:s");

$jsonExample = [
	["id" => 1, "status" => 'warning', "event" => 'ronda vuelta de forma recta.', "person" => 'Jose Hernandez', "created_at" => $now],
	["id" => 2, "status" => 'success', "event" => 'test comprobando activo', "person" => 'Pedro Hernandez', "created_at" => $now],
	["id" => 3, "status" => 'danger', "event" => 'test no respondio.', "person" => 'Jose Martinez', "created_at" =>  $now],
	["id" => 4, "status" => 'primary', "event" => 'esperando terminar ronda', "person" => 'Ivan fernandez', "created_at" => $now],
	["id" => 5, "status" => 'warning', "event" => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?', "person" => 'Jose Hernandez', "created_at" => $now],
];
$aux = [];
$data = [];
$id = 0;
if (isset($_GET["id"])) {
	$id = $_GET["id"];
}
$numberRecords = random_int(0, 100);
for ($i = 0; $i < $numberRecords; $i++) {
	$randomIndex =  random_int(0, (count($jsonExample) - 1));
	$jsonExample[$randomIndex]["id"] = $id + $i + 1;
	array_unshift($aux, $jsonExample[$randomIndex]);
}
/* $data = $aux; */

/*  $eventos->read()->fetchAll(PDO::FETCH_CLASS, "EventosModel");
 */ 
 $data =  $eventos->read()->fetchAll(PDO::FETCH_CLASS, "EventosModel");
$response = compact('status', 'data');


Response::json($response);
