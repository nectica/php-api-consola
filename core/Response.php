<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class Response {
    
    private $response ;
    public function __construct($response)
    {
        $this->response = $response;        
    }
    public static function response($response){
        http_response_code(202);
        echo $response;
    }
    public static function json($response){
        http_response_code(202);
        echo json_encode( $response);
    }
}