<?php

include './../database/Database.php';

class DeviceModel {
    private $conn;
    private $table = "";
    public $id, $id_status, $id_legajo, $nombre_operador, $id_objectivo,
    $nombre_objectivo, $tipo_evento, $status, $fecha; 

    function __construct()
    {
        $database = new Database;
        $this->conn = $database->getConnection();
    }
    public function read(){

        $query = "
            SELECT *
            FROM ".$this->table;
        ;
        $stmt = $this->conn->query($query);
        $stmt->execute();
        return $stmt;
    }
}